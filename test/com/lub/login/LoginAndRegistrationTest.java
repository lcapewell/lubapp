/**
Created by Leanne Capewell
Email: capewell.leanne@gmail.com
Created on 08/09/2013
*/

package com.lub.login;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;

import android.test.InstrumentationTestCase;

import com.lub.friend.database.UpdateUser;
import com.lub.user.Person;
import com.parse.ParseUser;


/**
 * Unit tests for the LoginAndRegistration class.
 *
 */
public class LoginAndRegistrationTest extends InstrumentationTestCase {

	private LoginAndRegistration loginAndRegistration;
	private Person person;
	private ParseUser userMock;
	private UpdateUser updateUserMock;
	
	/**
	 * @throws java.lang.Exception
	 */
	public void setUp() throws Exception {
		super.setUp();
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		userMock = mock(ParseUser.class);
		updateUserMock = mock(UpdateUser.class);
		loginAndRegistration = new LoginAndRegistration(updateUserMock);
	}

	/**
	 * Test the register method
	 * @throws Exception
	 */
	@Test
	public final void testRegister() throws Exception {
		when(updateUserMock.getUser(person)).thenReturn(userMock);
		doNothing().when(userMock).signUp();
		loginAndRegistration.register(person);
		verify(userMock).signUp();
	}

}
