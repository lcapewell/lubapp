package com.lub.ui;

import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.lub.MenuActivity;
import com.lub.friend.activities.FriendsListActivity;
import com.lub.login.LoginActivity;

/**
 *Created by Leanne Capewell
 *Email: capewell.leanne@gmail.com
 *Created on 05/10/2013
 */

/**
 * Functional tests for the Friend related screens
 * 
 * Checks that the friend button works and the list of friends correctly display
 * 
 */
public class FriendsActivityTest extends
		ActivityInstrumentationTestCase2<LoginActivity> {

	private Solo solo;
	private String username;
	private String password;
	private String loginButton;
	private String friendName;
	private String friendsButton;
	private String logoutButton;

	public FriendsActivityTest() {
		super(LoginActivity.class);

	}

	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
		username = "testuser";
		password = "testpassword";
		friendsButton = "Friends";
		friendName = "Friend Test";
		loginButton = "Login";
		logoutButton = "Logout";
	}

	/**
	 * Login to the app, and proceed to the friends screen.
	 */
	public void testFriends() {

		// Login
		solo.enterText(0, username);
		solo.enterText(1, password);
		solo.clickOnButton(loginButton);
		solo.assertCurrentActivity(
				"MenuActivity not displayed after valid login",
				MenuActivity.class);

		// Open friends list view
		solo.clickOnButton(friendsButton);
		solo.assertCurrentActivity(
				"FriendsList activity not displayed after 'Friends' is pressed",
				FriendsListActivity.class);
		assertTrue(solo.searchText(friendName));
		solo.goBack();
		solo.clickOnButton(logoutButton);
	}

}
