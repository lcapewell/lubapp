
package com.lub.parse;

import java.util.List;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

/**
Created by Leanne Capewell
Email: capewell.leanne@gmail.com
Created on 25/09/2013
*/
public class ParseWrapper {

    public static final String FRIENDS_REQUESTS = "friends_requests";
    public static final String FRIENDS = "friends";
    public static final String FRIEND_ACKNOWLEDGE = "friends_acknowledge";
    public static final String PUBLICDATA = "publicdata";
    
/**
 * Wrapper for the ParseUser.getCurrentUser() static method
 * @return ParseUser object of the current user
 */
    public ParseUser getCurrentUser(){
		ParseUser user = ParseUser.getCurrentUser();
		return user;
	}
    
    public ParseRelation getRelation(ParseObject o, String relation)
    {
        return o.getRelation(relation);

    }
    
    public String getString(ParseObject o, String key) {
        return o.getString(key);
    }

    /**
     * Gets the given user from the database
     * @param username Username of user to be retrieved
     * @return ParseUser object for that username, or null if none is found
     */
    public ParseObject getUser(String username) {
        ParseQuery<ParseUser> q = ParseUser.getQuery();
        q.whereEqualTo("username", username);
        try {
            List<ParseUser> data = q.find();
            if(data.size()<1) {
                //User not found
                return null;
            }
            return data.get(0);

        } catch (ParseException e) {
            e.printStackTrace(); 
        }
        return null;
    }
	
}
