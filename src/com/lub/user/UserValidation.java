package com.lub.user;


/**
 * @author Leanne Capewell Email: capewell.leanne@gmail.com
 */
public class UserValidation {

	public static int minUsernameLength = 5;
	public static int minPasswordLength = 6;
	public static int minEmailLength = 5;

	/**
	 * Checks if a username is longer than the minimum required
	 * 
	 * @param username
	 *            THe username to check
	 * @return true if the username is valid
	 */
	public static boolean validateUsername(String username) {
		if (username.length() < minUsernameLength) {
			return false;
		}
		return true;
	}

	/**
	 * Checks if a password is longer than the minimum required
	 * 
	 * @param password
	 *            The password to check
	 * @return true if the password is valid
	 */
	public static boolean validatePassword(String password) {
		if (password.length() < minPasswordLength) {
			return false;
		}
		return true;
	}

	/**
	 * Checks if an email address is longer than the minimum required Does not
	 * check if the string is a valid email, but this is instead done by parse
	 * 
	 * @param email The email to validate
	 * @return true if the email is valid
	 */
	public static boolean validateEmail(String email) {
		if (email.length() < minEmailLength) {
			return false;
		}
		return true;
	}
}
