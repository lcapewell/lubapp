package com.lub.user;

import com.lub.R;
import com.parse.ParseUser;

/**
 * Created with IntelliJ IDEA. User: BenedictHobart Date: 21/08/13 Time: 11:20
 * 
 * Person is a model object used for holding details about a user.
 * 
 */
public class Person {
	private String username;
	private String password;
	private String email;
	private String firstName;
	private String lastName;
	private String address;
	private String phoneNumber;
	private int status;
	private boolean sentMessage;
	private boolean carer;

	public Person() {
		username = "";
		email = "";
		firstName = "";
		lastName = "";
		address = "";
		phoneNumber = "";
		status = R.string.ok;
		sentMessage = true;
		carer = false;
    }

	public Person(ParseUser user) {
		username = user.getString("username");
		email = user.getString("email");
		firstName = user.getString("first_name");
		lastName = user.getString("last_name");
		address = user.getString("address");
		phoneNumber = user.getString("phone_number");
		status = user.getInt("status");
		sentMessage = user.getBoolean("sentOk");
		carer = user.getBoolean("carer");
	}
    
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

	public boolean isSentMessage() {
		return sentMessage;
	}

	public void setSentMessage(boolean sentMessage) {
		this.sentMessage = sentMessage;
	}

	public boolean isCarer() {
		return carer;
	}

	public void setCarer(boolean carer) {
		this.carer = carer;
	}

}
