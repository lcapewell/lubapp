/**
Created by Leanne Capewell
Email: capewell.leanne@gmail.com
Created on 22/09/2013
*
* Displays the user's details and allows them to be edited and updated
*/
package com.lub.user;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.lub.R;
import com.lub.friend.database.UpdateUser;

public class ProfileActivity extends Activity {
    private EditText firstname;
    private EditText lastname;
    private EditText address;
    private EditText email;
    private EditText phoneNumber;
    private CheckBox checkCarer;
    private Button save;
    private Button cancel;
    private Person person;
    private UpdateUser updateUser = new UpdateUser();
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_profile);
        
        person =  updateUser.getCurrentPerson();
        
        firstname = (EditText)findViewById(R.id.first_name_profile);
        firstname.setText(person.getFirstName());
        
        lastname = (EditText)findViewById(R.id.last_name_profile);
        lastname.setText(person.getLastName());
        
        address = (EditText)findViewById(R.id.address_profile);
        address.setText(person.getAddress());
        
        email = (EditText)findViewById(R.id.email_profile);
        email.setText(person.getEmail());
        
        phoneNumber = (EditText)findViewById(R.id.phone_number_profile);
        phoneNumber.setText(person.getPhoneNumber());
        
        checkCarer = (CheckBox)findViewById(R.id.carer_check);
        checkCarer.setChecked(person.isCarer());
        
      // Save the user's new details when save button is pressed
        save = (Button)findViewById(R.id.save_profile);
        save.setOnClickListener(new View.OnClickListener() {
        	
            @Override
            public void onClick(View v) {
            	updatePerson();
            	updateUser.updateCurrentUser(person);
                finish();
            }
        });
        
        // Return to menu screen if cancel is tapped
        cancel = (Button)findViewById(R.id.cancel_profile);
        cancel.setOnClickListener(new View.OnClickListener() {
        	
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    // Creates a person object based on the text inputed into the fields
    private void updatePerson(){
        person.setFirstName(firstname.getText().toString());
        person.setLastName(lastname.getText().toString());
        person.setEmail(email.getText().toString());
        person.setPhoneNumber(phoneNumber.getText().toString());
        person.setAddress(address.getText().toString());
        person.setCarer(checkCarer.isChecked());
    }
}
