package com.lub.login;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.lub.R;
import com.lub.user.Person;
import com.lub.user.UserValidation;
import com.parse.ParseException;

/**
 * Created by: BenedictHobart Date: 26/08/13 Time: 11:17
 * 
 * Activity for the user registration view
 * Allows a new user to register for the application
 */
public class RegisterActivity extends Activity {
	private EditText username;
	private EditText password;
	private EditText email;
	private EditText firstName;
	private EditText lastName;
	private EditText address;
	private EditText phoneNumber;
	private CheckBox carerCheck;
	private Button register;
	private LoginAndRegistration loginAndRegistration = new LoginAndRegistration();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		username = (EditText) findViewById(R.id.username_register);
		password = (EditText) findViewById(R.id.password_register);
		email = (EditText) findViewById(R.id.email_register);
		firstName = (EditText) findViewById(R.id.first_name_register);
		lastName = (EditText) findViewById(R.id.last_name_register);
		address = (EditText) findViewById(R.id.address_register);
		phoneNumber = (EditText) findViewById(R.id.phone_number_register);
		carerCheck = (CheckBox) findViewById(R.id.carer_check);
		register = (Button) findViewById(R.id.register_register);
		register.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				registerPerson();
			}
		});
	}

	// Register a person as a new parseUser on the Parse database
	// Validates the text fields to ensure a valid username, password, and email
	// have been entered by the user
	
	private void registerPerson() {
		String userString = username.getText().toString().replaceAll("\\s+","");
		String passString = password.getText().toString().replaceAll("\\s+","");
		String emailString = email.getText().toString();

		// Validation
		// Username length
		if (!UserValidation.validateUsername(userString)) {
			Toast.makeText(getApplicationContext(),
					R.string.invalid_username_short, Toast.LENGTH_SHORT).show();
			return;
		}
		
		// Password length 
		if (!UserValidation.validatePassword(passString)) {
			Toast.makeText(getApplicationContext(),
					R.string.invalid_password_short, Toast.LENGTH_SHORT).show();
			return;
		}
		
		// Email length
		if (!UserValidation.validateEmail(emailString)) {
			Toast.makeText(getApplicationContext(), R.string.email_too_short,
					Toast.LENGTH_SHORT).show();
			return;
		}

		// Register the user
		Person person = createNewPerson();

		try {
			loginAndRegistration.register(person);
			makeToast(getString(R.string.registration_success_toast));
			finish();
		// Deal with any exceptions that caused registration to fail
		} catch (ParseException e) {
			int errorCode = e.getCode();
			
			// username already taken error
			if (errorCode == ParseException.USERNAME_TAKEN) {
				makeToast(getString(R.string.username_taken_toast));
			} 
			
			// email already taken error
			else if (errorCode == ParseException.EMAIL_TAKEN) {
				makeToast(getString(R.string.email_in_use_toast));
			} 
			
			// invalid email error
			else if (errorCode == ParseException.INVALID_EMAIL_ADDRESS) {
				makeToast(getString(R.string.email_invalid_toast));
			} 
			
			// no internet connection error
			else if (errorCode == ParseException.CONNECTION_FAILED) {
				makeToast(getString(R.string.no_internet_connection));
			} 
			
			// general registration error
			else{
				makeToast(getString(R.string.registration_failed_toast));
			}
		}

	}

	// Make a new person object based on the strings in the text fields
	// that have been entered by the user
	private Person createNewPerson() {
		Person person = new Person();
		person.setUsername(username.getText().toString());
		person.setPassword(password.getText().toString());
		person.setEmail(email.getText().toString());
		person.setAddress(address.getText().toString());
		person.setFirstName(firstName.getText().toString());
		person.setLastName(lastName.getText().toString());
		person.setPhoneNumber(phoneNumber.getText().toString());
		person.setCarer(carerCheck.isChecked());
		return person;
	}

	// Make a toast alert message
	private void makeToast(String s) {
		Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
		return;
	}
}
