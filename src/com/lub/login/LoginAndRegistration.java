/**
 * Created by Leanne Capewell 
 * Email: capewell.leanne@gmail.com
 * Created on 08/09/2013
 */

package com.lub.login;

import com.lub.friend.database.UpdateUser;
import com.lub.user.Person;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

/**
 * 
* Created by Leanne Capewell
* Email: capewell.leanne@gmail.com
* Created on 20/10/2013
 */
public class LoginAndRegistration {
	private UpdateUser updateUser;
	
	
	public LoginAndRegistration() {
		updateUser = new UpdateUser();
	}

	/**
	 * Test Constructor
	 */
	public LoginAndRegistration(UpdateUser updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * Registers a person the database
	 * 
	 * Creates a user object for the given Person object, then uses this to
	 * signUp with the parse database.
	 * 
	 * If signUp fails, a ParseException is thrown detailing the reason for the
	 * failure (eg invalid email address)
	 * 
	 * person - the person to register
	 */
	public void register(Person person) throws ParseException {
		ParseUser user = updateUser.getUser(person);
		user.signUp();
	}

	/**
	 * Log user into application
	 * 
	 * Validates user's credentials against the server. Returns true for
	 * successful login, false if it fails
	 * @throws ParseException 
	 */
	public void login(String username, String password) throws ParseException {
		ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
		parseInstallation.put("username", username);
		parseInstallation.saveInBackground();
		ParseUser.logIn(username, password);
	}

}
