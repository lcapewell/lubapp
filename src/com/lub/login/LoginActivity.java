package com.lub.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lub.MenuActivity;
import com.lub.R;
import com.lub.user.UserValidation;
import com.parse.ParseException;

/**
 * Activity for the Login layout
 * 
 * Allows a user to login to the application
 */
public class LoginActivity extends Activity {
	private Button login;
	private Button register;
	private EditText username;
	private EditText password;
	private LoginAndRegistration loginAndRegistration = new LoginAndRegistration();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		username = (EditText) findViewById(R.id.username_login);
		password = (EditText) findViewById(R.id.password_login);

		// If login button is tapped, login user and go to menu screen if
		// successful
		login = (Button) findViewById(R.id.login_login);
		login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (checkLogin()) {
					Intent i;
					i = new Intent(LoginActivity.this, MenuActivity.class);
					startActivity(i);
				} else {
					return;
				}
			}

		});
		// Go to register screen when Register button tapped
		register = (Button) findViewById(R.id.register_login);
		register.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i;
				i = new Intent(LoginActivity.this, RegisterActivity.class);
				startActivity(i);
			}
		});
	}

	/*
	 * Checks if login credentials are correct
	 * 
	 * Validates login and username, and attempts to login to database. Returns
	 * True if Login information is correct. If information entered is wrong or
	 * too short, it will display error message
	 */
	private boolean checkLogin() {
		String userString = username.getText().toString().replaceAll("\\s+","");
		String passString = password.getText().toString().replaceAll("\\s+","");

		// Validation
		// Username input validation
		if (!UserValidation.validateUsername(userString)) {
			Toast.makeText(getApplicationContext(),
					R.string.invalid_username_short, Toast.LENGTH_SHORT).show();
			return false;
		}
		// Password input validation
		if (!UserValidation.validatePassword(passString)) {
			Toast.makeText(getApplicationContext(),
					R.string.invalid_password_short, Toast.LENGTH_SHORT).show();
			return false;
		}
		// Login user
		try {
			loginAndRegistration.login(userString, passString);
			return true;
		}
		// Return error message is login fails
		catch (ParseException e) {
			if (e.getCode() == ParseException.CONNECTION_FAILED) {
				Toast.makeText(getApplicationContext(),R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
				return false;
			} else {
				Toast.makeText(getApplicationContext(),	R.string.invalid_login_toast, Toast.LENGTH_SHORT).show();
				return false;
			}
		}
	}
}
