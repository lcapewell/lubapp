package com.lub.friend.activities;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lub.R;
import com.lub.friend.database.FriendGroup;
import com.lub.friend.model.Friend;

/**
 * Created with IntelliJ IDEA.
 * User: BenedictHobart
 * Date: 28/08/13
 * Time: 9:34 AM
 */

/**
 * FriendsListFragment is a ListFragment populated using fragment_friend as the layout for each element
 */
public class FriendsListFragment extends ListFragment {
    //Friend list used by the Array adapter
    private ArrayList<Friend> friends;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Get list of friends
        friends = FriendGroup.get().getFriends();

        //Create List adapter
        FriendAdapter adapter = new FriendAdapter(friends);
        setListAdapter(adapter);


    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //Get the friend associated with List Item clicked
        Friend f = ((FriendAdapter)getListAdapter()).getItem(position);

        //Setup Intent for Fried detail
        Intent i = new Intent(getActivity(),FriendDetailActivity.class);
        i.putExtra(FriendDetailActivity.EXTRA_FRIEND_ID,f.getId());
        startActivity(i);
    }
    private class FriendAdapter extends ArrayAdapter<Friend> {
        public FriendAdapter(ArrayList<Friend> friends) {
            super(getActivity(),0,friends);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            //setup fragment layout objects
            if(convertView==null) {
                //Infate layout
                convertView = getActivity().getLayoutInflater().inflate(R.layout.fragment_friend,null);
            }
            //Get friend
            Friend friend = getItem(position);
            TextView name = (TextView)convertView.findViewById(R.id.name_friend);
            String nameText = friend.getFirstName() + " " + friend.getLastName();
            if (nameText.equals(" ")){
            	name.setText(friend.getUsername());
            }
            else{
            	name.setText(nameText);
            }
            LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.layout_friend);
            layout.setBackgroundResource(friend.getColour());
            TextView address = (TextView)convertView.findViewById(R.id.detail_friend);
            address.setText(friend.getAddress());
            return convertView;
        }
    }
}
