package com.lub.friend.activities;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.lub.R;
import com.lub.friend.database.Request;
import com.lub.friend.database.RequestHandler;
import com.lub.friend.database.Requests;

/**
 * Created with IntelliJ IDEA.
 * User: BenedictHobart
 * Date: 28/08/13
 * Time: 9:34 AM
 * To change this template use File | Settings | File Templates.
 */
/**
 * RequestListFragment is a ListFragment populated using fragment_request as the layout for each element
 */
public class RequestListFragment extends ListFragment {
    //requests list used by list adapter
    private ArrayList<Request> requests;
    private RequestHandler requestHandler;
    private Request request;
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setup requests
        requests = Requests.get().getRequests();
        requestHandler = new RequestHandler();

        //create request adapter
        RequestAdapter adapter = new RequestAdapter(requests);
        setListAdapter(adapter);
    }
    
    private class RequestAdapter extends ArrayAdapter<Request> {
        public RequestAdapter(ArrayList<Request> requests) {
            super(getActivity(), 0, requests);
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //Inflate layout
            if(convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.fragment_request,null);
            }
            //Get request
            request = getItem(position);

            //Setup layout objects
            TextView name = (TextView)convertView.findViewById(R.id.request_username_text);
            name.setText(request.getName());
            Button yes = (Button)convertView.findViewById(R.id.request_yes_button);

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestHandler.acceptRequest(request);
                    getActivity().recreate();
                }
            });
            Button no = (Button)convertView.findViewById(R.id.request_no_button);
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestHandler.declineRequest(request);
                    getActivity().recreate();
                }
            });
            return convertView;
        }
    }
}
