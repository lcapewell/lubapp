package com.lub.friend.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lub.R;
import com.lub.friend.database.FriendGroup;
import com.lub.friend.database.Requests;
import com.lub.user.UserValidation;

/**
 * User: BenedictHobart
 * Date: 26/08/13
 * Time: 12:17 PM
 */

/**
 * This activity lists the current users friends in a readable and interactive
 * manner it populates the list with a FriendListFragment and a
 * RequestListFragment
 */
public class FriendsListActivity extends FragmentActivity {
    //Layout objects
	private Button addFriendButton;
	private EditText addFriendText;
	private Button requestsButton;
	private Button refreshButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friends_list);

        //Check for recent changes to users friends
        FriendGroup.get().update();

		FragmentManager fm = getSupportFragmentManager();
		Fragment friendFragment = fm.findFragmentById(R.id.friendContainer);

		//Create Friend fragment
        if (friendFragment == null) {
			friendFragment = new FriendsListFragment();
			fm.beginTransaction().add(R.id.friendContainer, friendFragment)
					.commit();
		}

		//Setup layout objects
		addFriendText = (EditText) findViewById(R.id.add_friend_text);
		addFriendButton = (Button) findViewById(R.id.add_friend_button);
		addFriendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d("FriendsListFragment", addFriendText.getText().toString());
				String friendUsername = addFriendText.getText().toString().replaceAll("\\s+","");
				if (UserValidation.validateUsername(friendUsername)) {
					if (Requests.get().friendRequest(friendUsername) == false) {
						Toast.makeText(getApplicationContext(), R.string.friend_doesnt_exist, Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(getApplicationContext(), R.string.friend_request_sent, Toast.LENGTH_SHORT).show();
					}
				}
				else{
					Toast.makeText(getApplicationContext(), R.string.invalid_username_short, Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		requestsButton = (Button) findViewById(R.id.requests_button);
		requestsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Requests.get().update();
				Intent i = new Intent(FriendsListActivity.this,
						RequestListActivity.class);
				startActivity(i);
			}
		});

		refreshButton = (Button) findViewById(R.id.refresh_button);
		refreshButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FriendGroup.get().update();
				recreate();
			}
		});
	}
}
