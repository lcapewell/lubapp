package com.lub.friend.database;

import com.lub.friend.model.Friend;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * User: BenedictHobart
 * Date: 27/08/13
 * Time: 11:08 AM
 * 
 * Retrieves and stores a list of the user's friends from the database
 */
public class FriendGroup {
    public static FriendGroup friendGroup;
    private ArrayList<Friend> friends;

    /**
     * removes references to the friendGroup so it can be collected by the JVM garbage collector
     */

    public static void destroy() {
        friendGroup = null;
    }

    /**
     * Returns the singleton instance of FriendGroup
     * @return
     * friendGroup
     */
    public static FriendGroup get() {
        if (friendGroup == null) {
            friendGroup = new FriendGroup();
        }
        return friendGroup;
    }

    /**
     * creates a new FriendGroup initializes and populates the friends ArrayList
     * using the update() function
     */
    public FriendGroup() {
        update();
    }

    /**
     * retrieves the friends relation from the server for the currently logged in user
     * and populates the friends arraylist
     */
    public void update() {
        friends = new ArrayList<Friend>();
        ParseUser user = ParseUser.getCurrentUser();
        ParseRelation<ParseObject> friendsRelation = user.getRelation("friends");
        try {
            List<ParseObject> friendsList = friendsRelation.getQuery().find();
            for(int i = 0; i < friendsList.size(); ++i) {
                friends.add(new Friend(friendsList.get(i)));
            }
            user.save();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * returns a friend based on the given friendId
     * @param friendId unique id for that friend
     * @return null if no matching user is found
     */
    public Friend getFriend(String friendId) {
    	for (Friend friend : friends){
            if(friend.getId().equals(friendId)) {
                return friend;
            }
        }
        return null;
    }

    /**
     * returns the ArrayList of friends for the current user
     * @return
     */
    public ArrayList<Friend> getFriends() {
        return friends;
    }
}
