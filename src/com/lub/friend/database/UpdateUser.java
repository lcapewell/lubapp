/**
 * @author Leanne Capewell 
 * Email: capewell.leanne@gmail.com
 * 
 * Updates a user in the database
 */
package com.lub.friend.database;

import com.lub.parse.ParseWrapper;
import com.lub.user.Person;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * @author Leanne Capewell 
 * Email: capewell.leanne@gmail.com
 * 
 * Updates a user in the database
 */
public class UpdateUser {

	private ParseWrapper parseWrapper;
	
	/**
	 * Constructor
	 */
	public UpdateUser(){
		this.parseWrapper = new ParseWrapper();
	}
	
	/**
	 * Test Constructor
	 */
	public UpdateUser(ParseWrapper parseWrapper){
		this.parseWrapper = parseWrapper;
	}
	
	/**
	 * Update a user's status to the given value
	 * 
	 * @param status The new status
	 */
	public void setStatus(int status) {
		ParseUser user = parseWrapper.getCurrentUser();
		user.put("status", status);
		user.put("sentOk",true);
		user.saveInBackground();
	}

	/*
	 * Updates the current user on the server using attributes in the person
	 * object
	 * 
	 * Only updates the attributes that have changed
	 * 
	 * @param person Person object containing the updated details
	 */
	public boolean updateCurrentUser(Person person) {
		ParseUser user = parseWrapper.getCurrentUser();

		if (!person.getEmail().equals(user.getEmail())) {
			user.setEmail(person.getEmail());
		}
		if (!person.getEmail().equals(user.get("first_name"))) {
			user.put("first_name", person.getFirstName());
		}
		if (!person.getLastName().equals(user.get("last_name"))) {
			user.put("last_name", person.getLastName());
		}
		if (!person.getAddress().equals(user.get("address"))) {
			user.put("address", person.getAddress());
		}
		if (person.getStatus() != user.getInt("status")) {
			user.put("status", person.getStatus());
		}
		if (!person.getPhoneNumber().equals(user.get("phoneNumber"))) {
			user.put("phoneNumber", person.getPhoneNumber());
		}
		if (person.isCarer() != user.getBoolean("carer")) {
			user.put("carer", person.isCarer());
		}
		try {
			user.save();
			return true;
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
	}

	/*
	 * Get the current user from the database within a Person object
	 * 
	 * @return person The current user's details
	 */
	public Person getCurrentPerson() {
		ParseUser user = parseWrapper.getCurrentUser();
		Person person = new Person();
		person.setEmail(user.getEmail());
		person.setFirstName((String) user.get("first_name"));
		person.setLastName((String) user.get("last_name"));
		person.setAddress((String) user.get("address"));
		person.setStatus(user.getInt("status"));
		person.setPhoneNumber((String) user.get("phoneNumber"));
		person.setSentMessage(user.getBoolean("sentOk"));
		person.setCarer(user.getBoolean("carer"));
		return person;
	}
	
	/**
	 * Create a ParseUser object from a Person object
	 * @param person
	 * @return
	 */
	public ParseUser getUser(Person person) {
		ParseUser user = new ParseUser();
		user.setEmail(person.getEmail());
		user.setPassword(person.getPassword());
		user.setUsername(person.getUsername());
		user.put("first_name", person.getFirstName());
		user.put("last_name", person.getLastName());
		user.put("address", person.getAddress());
		user.put("status", person.getStatus());
		user.put("phoneNumber", person.getPhoneNumber());
		user.put("carer", person.isCarer());
		return user;
	}

	/**
	 * Returns the current user's status
	 * This can be ok, alert, or notOk
	 * @return The current user's status
	 */
	public int getCurrentStatus() {
		Person person = getCurrentPerson();
		return person.getStatus();
	}
	
	    /**
	     * destroys the model objects of all major subsystems to be repopulated on next user login
	     */
	    public void logOut() {
	        ParseUser.logOut();
	        FriendGroup.destroy();
	        Requests.destroy();
	    }
}
