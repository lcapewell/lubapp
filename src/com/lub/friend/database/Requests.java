package com.lub.friend.database;

import java.util.ArrayList;
import java.util.List;

import com.lub.parse.ParseWrapper;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

/**
 * User: BenedictHobart
 * Date: 11/09/13
 * Time: 11:38 AM
 * 
 * Handles requests related to adding, accepting, and declining friends
 */
public class Requests {

    public static final String TAG = "Requests";
    private static Requests requests;
    private ArrayList<Request> requestsList;

    /**
     * returns the array requestsList of requests for the current user
     * @return array requestsList of requests for the current user
     */
    public ArrayList<Request> getRequests() {
        if(requestsList == null) {
            requestsList = new ArrayList<Request>();
        }
        return requestsList;
    }

    /**
     * removes references to the Requests Object so it can be destroyed by the JVM Garbage collector
     */
    public static void destroy() {
        requests = null;
    }
    public static Requests get() {
        if(requests == null) {
            requests = new Requests();
        }
        return requests;
    }

    /**
     * Creates new requests object, should only be called by Requests.get()
     */
    public Requests() {
        update();
    }
    
    /**
     * confirm all acknowledgments
     */
    private boolean acceptAcknowledgements() {
        try {
            ParseObject pubData = getPublicData(ParseUser.getCurrentUser().getUsername());
            ParseRelation<ParseObject> acknowledgeRelation = pubData.getRelation(ParseWrapper.FRIEND_ACKNOWLEDGE);
            ParseRelation<ParseObject> friendsRelation = ParseUser.getCurrentUser().getRelation(ParseWrapper.FRIENDS);
            List<ParseObject> acknowledgmentsList = acknowledgeRelation.getQuery().find();

            for(int i = 0; i < acknowledgmentsList.size(); ++i) {
                friendsRelation.add(acknowledgmentsList.get(i));
                acknowledgeRelation.remove(acknowledgmentsList.get(i));
            }
            ParseUser.getCurrentUser().save();
            pubData.save();
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

    }
    
    /**
     * updates the requestsList of requests for the current user based on data from the server
     */
    public void update() {
        acceptAcknowledgements();
        requestsList = new ArrayList<Request>();
        ParseObject user = getPublicData(ParseUser.getCurrentUser().getUsername());
        ParseRelation<ParseObject> reqsRelation = user.getRelation(ParseWrapper.FRIENDS_REQUESTS);
        try {
            List<ParseObject> reqList = reqsRelation.getQuery().find();
            for(int i=0; i < reqList.size(); ++i) {
                requestsList.add(new Request(reqList.get(i)));
            }
            user.save();
        } catch (ParseException e) {
            e.printStackTrace(
            );
        }
    }

    /**
     *Send a friend request to the given user
     * @param username The username of the friend being added
     */
    public boolean friendRequest(String username) {
        try {
            ParseObject data = getPublicData(username);
            ParseRelation<ParseUser> relation = data.getRelation(ParseWrapper.FRIENDS_REQUESTS);
            relation.add(ParseUser.getCurrentUser());
            data.save();
            return true;

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Reject the friend request sent by the given person
     * @param person The person who sent the friend request
     */
    public boolean ignoreRequest(ParseObject person) {
        try {
            ParseObject data = getPublicData(ParseUser.getCurrentUser().getUsername());
            ParseRelation<ParseObject> relation = data.getRelation(ParseWrapper.FRIENDS_REQUESTS);
            relation.remove(person);
            data.save();
            return true;
        } catch (ParseException e) {
            e.printStackTrace(); 
            return false;
        }
    }
    
    
    private ParseObject getPublicData(String username) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseWrapper.PUBLICDATA);
        query.whereEqualTo("username", username);
        List<ParseObject> queries = null;
        ParseObject data = null;
        try {
            queries = query.find();
            if(queries.size() != 1) {
                ParseObject object = ParseObject.create(ParseWrapper.PUBLICDATA);
                object.put("username", username);
                data = object;
                data.save();
            }
            else {
                data = queries.get(0);
            }
        } catch (ParseException e) {
            e.printStackTrace(); 
        }
        return data;
    }
    
    /**
     * Accept a users friend request
     * @param friend The user who sent the friend request
     * @return true is successful
     */
    public boolean acknowledgeRequest(ParseObject friend) {
            //Get Public Data
            ParseObject data = getPublicData(friend.getString("username"));

            //Tell Friend Added
            ParseRelation<ParseObject> relation = data.getRelation(ParseWrapper.FRIEND_ACKNOWLEDGE);
            relation.add(ParseUser.getCurrentUser());

            //Add Friend
            relation = ParseUser.getCurrentUser().getRelation(ParseWrapper.FRIENDS);
            relation.add(friend);
        try {
            data.save();
        } catch (ParseException e) {
            e.printStackTrace(); 
            return false;
        }

        //Remove Request
            data = getPublicData(ParseUser.getCurrentUser().getUsername());
            relation = data.getRelation(ParseWrapper.FRIENDS_REQUESTS);
            relation.remove(friend);
        try {
            ParseUser.getCurrentUser().save();
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        try {
            data.save();
        } catch (ParseException e) {
            e.printStackTrace(); 
            return false;
        }
        return true;
    }
}
