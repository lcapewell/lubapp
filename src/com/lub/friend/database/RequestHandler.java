
/**
 * 
 */
package com.lub.friend.database;

import com.lub.notification.NotificationService;

/**
 * Created by Leanne Capewell
 * Email: capewell.leanne@gmail.com
 * Created on 21/10/2013
 * 
 * Contains all the request related methods for sending, recieving, and acknowledging friend requests
 */
public class RequestHandler {
	
    private NotificationService notification;
    private Requests requests;
    
    public RequestHandler() {
    	notification = new NotificationService();
    	requests = Requests.get();
    }

    /**
     *  Test constructor
     */
	public RequestHandler(NotificationService notification, Requests requests) {
		this.notification = notification;
		this.requests = requests;
	}

	/**
     * Send a request to the given user
     * @param username The username of the user to be sent the request
     */
    public void sendRequest(String username) {
        if (requests.friendRequest(username)){
        notification.pushNotificationToUser(username, " has sent you a friend request");
        }
        destroy();
    }
    
    /**
     * Accept a friend request
     */
    public void acceptRequest(Request request) {
    	requests.acknowledgeRequest(request.getRequester());
        destroy();
    }
    
    /**
     * Decline a friend request
     */
    public void declineRequest(Request request)  {
    	requests.ignoreRequest(request.getRequester());
        destroy();
    }
    
    /**
     * Destroy requests objects
     */
    private void destroy() {
    	requests.getRequests().remove(this);
    }
}
