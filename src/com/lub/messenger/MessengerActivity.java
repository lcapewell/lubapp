package com.lub.messenger;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lub.R;

/**
 * User: Jordan Burdett 
 * Email: jburdett@student.unimelb.edu.au 
 * Date: 25/09/13
 * 
 * The messenger activity. Currently only displays an acknowledgement that ok
 * message has been sent Clicking ok button returns user to menu activity
 * 
 */
public class MessengerActivity extends Activity {
	Button confirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set the layout
		setContentView(R.layout.activity_messenger);

		// Link to FriendsList when friend button is tapped
		confirm = (Button) findViewById(R.id.ok_button);
		confirm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
