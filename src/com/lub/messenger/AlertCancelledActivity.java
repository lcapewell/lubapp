package com.lub.messenger;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lub.R;

/**
 * Created by Leanne Capewell 
 * Email: capewell.leanne@gmail.com 
 * Created on 03/10/2013
 * 
 * Displays an acknowledgment that their alert has been cancelled Clicking ok
 * button returns user to menu activity
 */
public class AlertCancelledActivity extends Activity {
	Button confirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set the layout
		setContentView(R.layout.activity_cancel_alert);

		// Link to FriendsList when friend button is tapped
		confirm = (Button) findViewById(R.id.ok_button);
		confirm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
