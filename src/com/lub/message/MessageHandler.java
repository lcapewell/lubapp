/**
 * Created by Leanne Capewell 
 * Email: capewell.leanne@gmail.com
 * Created on 19/09/2013
 */

package com.lub.message;

import com.lub.R;
import com.lub.friend.database.UpdateUser;
import com.lub.notification.NotificationService;
import com.lub.user.Person;

/**
 * 
 * This class handles sending on various messages
 * 
 * Sends alerts, ok messages, and not ok messages Also cancels alerts
 * 
 * Updates the user's status to reflect the type of message sent
 * 
 */
public class MessageHandler {

	private static final String NOT_OK_MESSAGE = " has not said they are ok today";
	private static final String OK_MESSAGE = " is doing well today";
	private static final String ALERT_MESSAGE = " has sent an alert";
	private static final String CANCEL_MESSAGE = " has cancelled their alert";
	private UpdateUser updateUser;
	private NotificationService notification;

	/**
	 * Normal constructor
	 */
	public MessageHandler() {
		this.updateUser = new UpdateUser();
		this.notification = new NotificationService();
	}

	/**
	 * Test constructor
	 */
	public MessageHandler(UpdateUser updateUser, NotificationService notificationService) {
		this.updateUser = updateUser;
		this.notification = notificationService;
	}

	/**
	 * Send an ok message notification to all friends and update user's status
	 * if the user's status is not already ok
	 * Returns true if message sends successfully
	 */
	public boolean sendOkMessage() {
		Person person = updateUser.getCurrentPerson();
		if(person.getStatus() != R.integer.ok){
			updateUser.setStatus(R.integer.ok);
		}
		notification.pushNotificationToAllFriends(OK_MESSAGE);
		return true;
	}

	/**
	 * Send a no ok message notification to all friends and update user's status
	 * to not ok
	 * 
	 */
	public void sendNoOkMessage() {
		updateUser.setStatus(R.integer.notOk);
		notification.pushNotificationToAllFriends(NOT_OK_MESSAGE);
	}

	/**
	 * Send an alert notification to all friends
	 * and update user's status to alert
	 */
	public void sendAlert() {
		updateUser.setStatus(R.integer.alert);
		notification.pushNotificationToAllFriends(ALERT_MESSAGE);
	}

	/** 
	* Send an alert cancelled notification to all friends
	* and update user's status to ok
	*/
	public void cancelAlert() {
		updateUser.setStatus(R.integer.ok);
		notification.pushNotificationToAllFriends(CANCEL_MESSAGE);
	}
}