Lub is an Android application for disabled or elderly users to be able to quickly and easily communicate
with their family and carers to provide them with assurance that they are ok.
It also allows users to inform their contacts if they are in need of help or assistance.


To run the project, import it in the Android/ directory in to a new ADT workspace.
Run using Android-17 SDK.
Run the project in an emulator or on an android device.

To log in to the app, you may register a new user, or use the following:
username: marysue
password: asdfgh

Tag: simple-MVC
---------------
The open source buttons can be seen in the main menu screen (after login)

Details are in the drawable-hdpi/custom_button.xml file
Also added launch and login screen icons/logo.

Testing
-------
Test files are found in test package.  Run as an Android JUnit test.
It is required that you have an emulator or device available to run tests.
These include unit tests and automated ui tests.
	JUnit has been used for unit testing
	Mockito has been used for mocking
	Robotium has been used for ui automation

