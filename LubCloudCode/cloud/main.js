Parse.Cloud.job("checkAssistedPersons", function(request, status) {
        var query = new Parse.Query(Parse.User);
        query.each(function(user) {
            // Update to plan value passed in
            var sentOk = user.get("sentOk");
            if(sentOk) {
                user.set("sentOk",false);
            } else {
                var friends = user.relation("friends");
                var username = user.get('username');
                user.set("status",1)
                friends.query().each(function(friend) {
                    var query = new Parse.Query(Parse.Installation);
                    var friendname = friend.get('username');
                    query.equalTo('username',friendname);
                    Parse.Push.send({
                      where: query, 
                      data: {
                        alert: username + " is in trouble!!!"
                      }
                    }, {
                      success: function() {
                        // Push was successful
                        
                      },
                      error: function(error) {
                        // Handle error
                      }
                    });
                })
            }
            user.save();
            return;
        }).then(function() {
                status.success("All User's Alerted");
            }, function() {
                status.error("Alerts Didn't Complete");
            }) 

});    
